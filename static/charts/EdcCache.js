var counter = 0
function EdcCache (cacheSize, storagetype) {
  counter++
  var self = this
  var timestampMap = {}; var sizeMap = {}
  var CacheLimit = 5242880
  var newKeySize, totalStorageSize, currentSize
  if (cacheSize > CacheLimit)	{ cacheSize = CacheLimit }
  this.storagetype = storagetype
  this.cacheSize = cacheSize

  this.add = function (key, keydatavalue)	{
    var timestammpmap = JSON.parse(storagetype.getItem('timestampMap' + counter))
    	var sizemap = JSON.parse(storagetype.getItem('sizeMap' + counter))
    var date = new Date()
	    currentSize = storagetype.getItem('currentSize' + counter)
	    if (currentSize === null) {
	    currentSize = 0
	    newKeySize = 0
	    currentSize += (key.length + keydatavalue.length) * 2
    } else {
      newKeySize = (key.length + keydatavalue.length) * 2
      currentSize = +currentSize + +newKeySize
	    }
    totalStorageSize = +currentSize + +newKeySize
    storagetype.setItem('currentSize' + counter, totalStorageSize)
    if (timestammpmap != null) {
      if (timestammpmap.length != 0 && sizemap.length != 0)		{
        timestammpmap[date] = key
        storagetype.setItem('timestampMap' + counter, JSON.stringify(timestammpmap))
	    sizemap[key] = keydatavalue.length
	    storagetype.setItem('sizeMap' + counter, JSON.stringify(sizemap))
      }
    }		else		{
      timestampMap[date] = key
      storagetype.setItem('timestampMap' + counter, JSON.stringify(timestampMap))
	    sizeMap[key] = keydatavalue.length
	    storagetype.setItem('sizeMap' + counter, JSON.stringify(sizeMap))
    }
	    while (totalStorageSize > cacheSize)			{
			 evict()
    }
	 storagetype.setItem(key, keydatavalue)
  }

  this.get = function (key) {
    	data = storagetype.getItem(key)
    	return data
  }

  this.remove = function (key, firstkey) {
    	timestampMap = JSON.parse(storagetype.getItem('timestampMap' + counter))
    	sizeMap = JSON.parse(storagetype.getItem('sizeMap' + counter))
    	storagetype.removeItem(key)
    	var currentkeysize = sizeMap[key]
    totalStorageSize = totalStorageSize - currentkeysize
    delete timestampMap[firstkey]
    delete sizeMap[key]
    storagetype.setItem('timestampMap' + counter, JSON.stringify(timestampMap))
    storagetype.setItem('sizeMap' + counter, JSON.stringify(sizeMap))
    storagetype.setItem('currentSize' + counter, totalStorageSize)
  }

  this.clearAll = function () {
    	storagetype.clear()
  }

  function evict () {
    	timestampMap = JSON.parse(storagetype.getItem('timestampMap' + counter))
    	sizeMap = JSON.parse(storagetype.getItem('sizeMap' + counter))
    	console.log(sizeMap)
    	var keys = Object.keys(timestampMap), firstkey
    	keys.sort()// sorting ascending
    	firstkey = keys[0]
    	var key = timestampMap[firstkey]
    	self.remove(key, firstkey)
  }
}

