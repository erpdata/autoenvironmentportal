/* import Vue from 'vue'
import config from '../../config.json' */
var titleV
var vaxistitle
var dataval
var label
var dropdownTrendVal
var dropdownPeriodVal
var uriPathlevel
var key
var jsonData
var charttype
var jsondata
var data
var count = 0
var match
var rowIndex
var keydata
var txt
var breadvalue = []
var cache
var isLoading = false
var chart
var periodvalue
var periodtype
var checkedValue/*
var Vue = require('vue'); */
/* import config from '../../config.json'
	import Simplert from 'vue2-simplert'
	import BreadCrumbs from '../Breadcrumbs.vue'
	import Vue from 'vue'
*/

function checkValue (t) {
  if (t.is(':checked')) {
    checkedValue = 'true'
  } else {
    checkedValue = 'undefined'
  }
}
function defaultChart (selectedvalue) {
	// alert("hi");
  $('#dateFrom').datepicker({
    numberOfMonths: 1,
    onSelect: function (selected) {
      var to = $('#dateTo').val()
      var from = selected
    }
  })

  $('#dateTo').datepicker({
    numberOfMonths: 1,
    onSelect: function (selected) {
      var from = $('#dateFrom').val()
      var to = selected
    }
  })

	// localStorage
  cache = new EdcCache(5242880, sessionStorage)
  $('#ULtab').show()
  $('.breadcrumb li').each(function () {
    if (count >= 1) {
      $('.breadcrumb:first').remove()
      $('.breadcrumbseparator:first').remove()
      $('.breadcrumb>li+li').css('display', 'none')
    } else if (count == 0) {
      $('.breadcrumb li:before').css('display', 'none')
      $(this).remove()
    }
  })

	// checkedValue = $('.messageCheckbox:checked').val();

	/**
	 * Todo
	 */
	// document.getElementById("show").addEventListener("click", showFun);
  function showFun () {
    var from = document.getElementById('dateFrom').value
    var to = document.getElementById('dateTo').value
    getChartData(from, to, checkedValue)
  }

  if (selectedvalue === '1DAY' || selectedvalue === '5DAY' ||
			selectedvalue === '1MONTH' || selectedvalue === '3MONTH' ||
			selectedvalue === '6MONTH' || selectedvalue === '1YEAR' ||
			selectedvalue === '5YEAR' || selectedvalue === 'Max') {
		// alert(checkedValue);
    var datefrom, dateto

    var today = new Date()
    var dd = today.getDate()
    var mm = today.getMonth() + 1 // January is 0!
    var yyyy = today.getFullYear()

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }
    dateto = yyyy + '-' + mm + '-' + dd + ' 23:59:59'

    if (selectedvalue === '1DAY') {
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === '5DAY') {
      var fiveday = today.setDate(today.getDate() - 5)
      var date = new Date(fiveday)
      var dd = date.getDate()
      var mm = date.getMonth() + 1 // January is 0!
      var yyyy = date.getFullYear()

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === '1MONTH') {
      var onemon = today.setDate(today.getDate() - 30)
      var date = new Date(onemon)

      var dd = date.getDate()
      var mm = date.getMonth() + 1 // January is 0!
      var yyyy = date.getFullYear()

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === '3MONTH') {
      var threemon = today.setDate(today.getDate() - 90)
      var date = new Date(threemon)
      var dd = date.getDate()
      var mm = date.getMonth() + 1 // January is 0!
      var yyyy = date.getFullYear()

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === '6MONTH') {
      var sixmon = today.setDate(today.getDate() - 180)
      var date = new Date(sixmon)

      var dd = date.getDate()
      var mm = date.getMonth() + 1 // January is 0!
      var yyyy = date.getFullYear()

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === '1YEAR') {
      var oneyear = new Date()
      var dd = oneyear.getDate()
      var mm = oneyear.getMonth() + 1 // January is 0!
      var yyyy = oneyear.getFullYear() - 1

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === '5YEAR') {
      var fiveyear = new Date()
      var dd = fiveyear.getDate()
      var mm = fiveyear.getMonth() + 1 // January is 0!
      var yyyy = fiveyear.getFullYear() - 5

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    } else if (selectedvalue === 'Max') {
      var Max = new Date()
      var dd = Max.getDate()
      var mm = Max.getMonth() + 1 // January is 0!
      var yyyy = Max.getFullYear() - 5

      if (dd < 10) {
        dd = '0' + dd
      }

      if (mm < 10) {
        mm = '0' + mm
      }
      datefrom = yyyy + '-' + mm + '-' + dd + ' 00:00:01'
    }

		// $('#containerdiv').hide();
		// $('#loaderdiv').show();
    $('#divcol>a').removeClass('active')
    jQuery('#' + selectedvalue + '').addClass('active')

		// key=selectedvalue;
    if (selectedvalue === 'Max') {
      periodvalue = 0
      periodtype = selectedvalue
    } else {
      periodvalue = selectedvalue.charAt(0)
      periodtype = selectedvalue.substr(1)
      if (periodtype === 'MONTH') {
        if (periodvalue === '1') { periodvalue = 30 } else if (periodvalue === '3') { periodvalue = 90 } else if (periodvalue === '6') { periodvalue = 180 }
      }
    }

    function showLoader () {
      $('#errmsg').hide()
      $('#chart').hide()
      $('#containerdiv').hide()
      $('#loaderdiv').show()
      setTimeout(function () {
        $('#progressbar').css('display', '')
      }, 1)
			// $("#progressbar").css("display", "");
    }

    function hideLoader () {
      $('#errmsg').hide()
      $('#chart').show()
      $('#containerdiv').show()
      $('#loaderdiv').hide()
      setTimeout(function () {
        $('#progressbar').css('display', 'none')
      }, 1000)
    }

    if (checkedValue === 'true' || checkedValue === 'false') {
      key = selectedvalue + ' ' + datefrom + ' ' + dateto + ' projection'
    } else {
      key = selectedvalue + ' ' + datefrom + ' ' + dateto
    }

    keydata = cache.get(key)
    if (keydata != null) {
      jsondata = JSON.parse(keydata)
      charttype = jsondata.type
      data = jsondata.data
    } else {
      if (checkedValue === 'true' || checkedValue === 'false') {
				// uriPathlevel = "getsnapshotchart?periodvalue="+ periodvalue+
				// "&periodtype="+ periodtype+ "&radio="+ checkedValue;
        uriPathlevel = 'getsnapshotchart?fromdate=' + datefrom +
						'&todate=' + dateto + '&periodvalue=' + periodvalue +
						'&periodtype=' + periodtype + '&radio=' +
						checkedValue
        showLoader()
        var jsonData = $.ajax({
          url: uriPathlevel,
          dataType: 'json',
				//	async : false,
          beforeSend: function () {
            showLoader()
          },
          error: function () {
            hideLoader()
          },
          success: function (data) {
            hideLoader()
          }
        }).responseText
        jsondata = JSON.parse(jsonData)
        charttype = jsondata.type
        data = jsondata.data

        var index = data.length - 1
        var dateval = data[index]
        var indexto = data.length - 2
        dateto = data[indexto]
        var indexfrom = data.length - 3
        datefrom = data[indexfrom]
				// var datefrom=dateval[0];
				// var dateto=dateval[dateval.length-1];
        key = selectedvalue + ' ' + datefrom + ' ' + dateto +
						' projection'
        cache.add(key, jsonData)
      } else {
				// uriPathlevel = "getsnapshotchart?"&periodvalue="+
				// periodvalue+ "&periodtype="+ periodtype+ "&radio="+
				// checkedValue;
        console.log(Vue)
        alert('dsf')
        uriPathlevel = 'getsnapshotchart?fromdate=' + datefrom +
						'&todate=' + dateto + '&periodvalue=' + periodvalue +
						'&periodtype=' + periodtype + '&radio=' +
						checkedValue

        showLoader()
        var jsonData = $.ajax({
          url: uriPathlevel,
          dataType: 'json',
          headers: { 'Authorization': 'some value' },
				 	async: false,
          beforeSend: function () {
            showLoader()
          },
          error: function () {
            hideLoader()
          },
          success: function (data) {
            hideLoader()
          }
        }).responseText
        jsondata = JSON.parse(jsonData)
        charttype = jsondata.type
        data = jsondata.data

        var index = data.length - 1
        var dateval = data[index]
        var indexto = data.length - 2
        dateto = data[indexto]
        var indexfrom = data.length - 3
        datefrom = data[indexfrom]
				// var datefrom=dateval[0];
				// var dateto=dateval[dateval.length-1];
        key = selectedvalue + ' ' + datefrom + ' ' + dateto
        cache.add(key, jsonData)
      }
    }

    $('body').on('click', 'a.tablinks', function () {
      var datavalue = $(this).attr('content')
      var htitle = $(this).attr('htitle')
      var vtitle = $(this).attr('vtitle')
      var label = $(this).attr('label');
      +OpenDivforBreadCrumb(null, datavalue, htitle, vtitle, label)
    })

    $('#ULtab')
				.append(
						'<li><a href="javascript:void(0)" class="tablinks" content="' +
								key +
								'" htitle="titleV" vtitle="vaxistitle" label="">Home</a></li>')
    if (charttype === 'linechart') { lineChartFun(data) } else			{ MixlineChartFun(data) }
		//
  }
	/*
	 * else if(selectedvalue==="DataGrowth") { function showLoader() {
	 * $('#chart').hide(); $('#containerdiv').hide(); $('#loaderdiv').show();
	 * setTimeout(function () { $("#progressbar").css("display", ""); }, 1); }
	 *
	 * function hideLoader() { $('#chart').show(); $('#containerdiv').show();
	 * $('#loaderdiv').hide(); setTimeout(function () {
	 * $("#progressbar").css("display", "none"); }, 1000); }
	 *
	 * uriPathlevel = "getDataGrowthChart"; showLoader(); var jsonData=$.ajax({
	 * url : uriPathlevel, dataType : "json", async : false, beforeSend:
	 * function () { showLoader(); }, error: function(){ hideLoader();},
	 * success: function (data) { hideLoader();} }).responseText;
	 *
	 * jsondata=JSON.parse(jsonData); charttype=jsondata.type;
	 * data=jsondata.data;
	 *
	 * var index=data.length-1; var dateval=data[index]; var
	 * indexto=data.length-2; dateto=data[indexto]; var indexfrom=data.length-3;
	 * datefrom=data[indexfrom]; MixlineChartFun(data);
	 *  }
	 */

  function legendClickFunction (tablename) {
    if (tablename != null) {
      selectedvalue = null
      key = tablename + ' ' + datefrom + ' ' + dateto
      keydata = cache.get(key)
      if (keydata != null) {
        jsondata = JSON.parse(keydata)
        charttype = jsondata.type
        data = jsondata.data
      } else {
        console.log(config.REPORT_URL + '/rest/JDReport/Snapshot')
        uriPathlevel = config.REPORT_URL + '/rest/JDReport/Snapshot' + tablename +
						'&periodvalue=' + periodvalue + '&periodtype=' +
						periodtype + '&fromdate=' + datefrom + '&todate=' +
						dateto
        var jsonData = $.ajax({
          url: uriPathlevel,
          dataType: 'json',
				//	async : false,
          beforeSend: function () {
            showLoader()
          },
          error: function () {
            hideLoader()
          },
          success: function (data) {
            hideLoader()
          }
        }).responseText

        jsondata = JSON.parse(jsonData)
        charttype = jsondata.type
        data = jsondata.data

        cache.add(key, jsonData)
      }

      $('#ULtab').append(
					'<li><a href="javascript:void(0)" class="tablinks" content="' +
							key +
							'" htitle="titleV" vtitle="vaxistitle" label="">' +
							tablename + '</a></li>')
      if (charttype === 'linechart') {
        lineChartFun(data)
      } else {
        barchartFun(data)
      }
    }
  }

  function multipleTableClickFunction (tablearray) {
    tablearray = [ 'F43092', 'F4211', 'F4311', 'F4311T' ]

    if (tablearray != null) {
      key = tablearray
      keydata = cache.get(key)
      if (keydata != null) {
        jsondata = JSON.parse(keydata)
        charttype = jsondata.type
        data = jsondata.data
      } else {
        uriPathlevel = 'getMultipleTableClickData?tablearray=' +
						tablearray + '&periodvalue=' + periodvalue +
						'&periodtype=' + periodtype
        var jsonData = $.ajax({
          url: uriPathlevel,
          dataType: 'json',
				//	async : false,
          beforeSend: function () {
            showLoader()
          },
          error: function () {
            hideLoader()
          },
          success: function (data) {
            hideLoader()
          }
        }).responseText

        jsondata = JSON.parse(jsonData)
        charttype = jsondata.type
        data = jsondata.data
        cache.add(key, JSON.stringify(jsonData))
      }

      $('#ULtab').append(
					'<li><a href="javascript:void(0)" class="tablinks" content="' +
							key +
							'" htitle="titleV" vtitle="vaxistitle" label="">' +
							tablearray.toString() + '</a></li>')
      if (charttype === 'linechart') {
        lineChartFun(data)
      } else {
        barchartFun(data)
      }
    }
  }

  function chartFunction (label, datefrom, dateto) {
    if (label != null) {
      selectedvalue = null
      key = label + ' ' + datefrom + ' ' + dateto
      keydata = cache.get(key)
      if (keydata != null) {
        jsondata = JSON.parse(keydata)
        charttype = jsondata.type
        data = jsondata.data
      } else {
        uriPathlevel = 'getChartData?selectedvalue=' + label
        var jsonData = $.ajax({
          url: uriPathlevel,
          dataType: 'json',
				//	async : false,
          beforeSend: function () {
            showLoader()
          },
          error: function () {
            hideLoader()
          },
          success: function (data) {
            hideLoader()
          }
        }).responseText

        jsondata = JSON.parse(jsonData)
        charttype = jsondata.type
        data = jsondata.data
        cache.add(key, jsonData)
      }

      $('#ULtab').append(
					'<li><a href="javascript:void(0)" class="tablinks" content="' +
							key +
							'" htitle="titleV" vtitle="vaxistitle" label="">' +
							label + '</a></li>')
      if (charttype === 'linechart') {
        lineChartFun(data)
      } else {
        barchartFun(data)
      }
    }
  }

  function getChartData (from, to, checkedValue) {
    if (from !== '' && to !== '') {
      uriPathlevel = 'getcustomData?fromDate=' + from + '&toDate=' + to +
					'&radio=' + checkedValue

      var jsonData = $.ajax({
        url: uriPathlevel,
        dataType: 'json',
			//	async : false,
        beforeSend: function () {
          showLoader()
        },
        error: function () {
          hideLoader()
        },
        success: function (data) {
          hideLoader()
        }
      }).responseText

      jsondata = JSON.parse(jsonData)
      charttype = jsondata.type
      data = jsondata.data

      cache.add(key, jsonData)
      key = from + ' ' + to
      cache.add(key, jsonData)
      if (charttype === 'linechart') {
        lineChartFun(data)
      } else if (charttype === 'Mixlinechart') {
        MixlineChartFun(data)
      } else {
        barchartFun(data)
      }
    }
  }

	/*
	 * $('#button').click(function () { alert('dd'); var selectedPoints =
	 * chart.getSelectedPoints(); alert(selectedPoints);
	 *
	 * if (chart.lbl) { chart.lbl.destroy(); } chart.lbl =
	 * chart.renderer.label('You selected ' + selectedPoints.length + ' points',
	 * 100, 60) .attr({ padding: 10, r: 5, fill:
	 * Highcharts.getOptions().colors[1], zIndex: 5 }) .css({ color: 'white' })
	 * .add(); });
	 */
  function lineChartFun (data) {
    var interval = 0
    var nodata = data[0]

    var index = data.length - 2
    var datetoarray = data[index]
    var todate = datetoarray[0]
    index = data.length - 3
    var datefromarray = data[index]
    var fromdate = datefromarray[0]
    if (nodata === 'no data') {
      if (selectedvalue != undefined) {
        document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
						selectedvalue + ' from ' + fromdate + ' to ' + todate
      } else if (label != undefined) {
        document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
						label + ' from ' + fromdate + ' to ' + todate
      } else {
        document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
						tablename + ' from ' + fromdate + ' to ' + todate
      }

      $('#errmsg').show()
      $('#button').hide()
      $('#ULtab').hide()
      $('#chart').hide()
    } else {
      var index = data.length - 1
      var dateval = data[index]
      index = data.length - 2
      var datetoarray = data[index]
      var todate = datetoarray[0]
      index = data.length - 3
      var datefromarray = data[index]
      var fromdate = datefromarray[0]
      var len = dateval.length
      if (len < 2) {
        len = 0
      } else {
        len = len - 1
      }
      delete data[data.length - 3]
      delete data[data.length - 2]
      delete data[data.length - 1]
      var j = 0
      for (var i in data) {
        j++
      }
      var length = j
      var val = checkArray(data)
      if (val) {
        $('#errmsg').hide()
        $('#button').show()
        $('#ULtab').show()
        $('#chart').show()

        if (len > 15) {
          interval = 15
        }

        var options = {
          chart: {
            renderTo: 'chart'
					/*
					 * events: { load: function(){ this.showLoading(); } }
					 */
          },
          legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            enabled: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
					// reversed: true,

          },

          xAxis: {
            min: 0,
            max: len,
            categories: dateval,

            scrollbar: {
              enabled: true
            },
            tickLength: 0,
            tickInterval: interval

          },

          yAxis: {
            title: {
              text: 'Record Counts'
            },
            reversedStacks: false,
            plotLines: [ {
              value: 0,
              width: 1,
              color: '#808080'
            } ]
          },

          tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>:<b>{point.y}</b><br/>'
					// valueDecimals: 2
          },

          title: {
            text: 'Record Counts from ' + fromdate + ' to ' +
								todate,
            style: {
              fontWeight: 'bold',
              'fontSize': '14px'

            },
            x: -20
          },

          plotOptions: {

            series: {
              allowPointSelect: true,

              cursor: 'pointer',
              events: {
                legendItemClick: function (event) {
                  var tablename = this.name
                  legendClickFunction(tablename)
                },
                click: function (event) {
                  var tablename = this.name
                  legendClickFunction(tablename)
                }
              }
            }
          },
          series: [ {
            color: Highcharts.getOptions().colors[2]
          } ]

        }

        for (i = 0; i < length; i++) {
          console.log(data[i])
          options.series[i] = data[i]
        }

        if (i == length) {
          chart = new Highcharts.Chart(options)
          chart.hideLoading()
        }

        $('.highcharts-xaxis-labels text').on('click', function () {
          var label = $(this).text()

          chartFunction(label, datefrom, dateto)
        })
      } else {
        if (selectedvalue != undefined) {
          document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
							selectedvalue +
							' from ' +
							fromdate +
							' to ' +
							todate
        } else if (label != undefined) {
          document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
							label + ' from ' + fromdate + ' to ' + todate
        } else {
          document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
							tablename + ' from ' + fromdate + ' to ' + todate
        }

        $('#errmsg').show()
        $('#button').hide()
        $('#ULtab').hide()
        $('#chart').hide()
				// $('#Datachart').hide();
      }
    }
  }
  function checkArray (my_arr) {
    var j = 0
    for (var i in my_arr) {
      j++
    }
    var length = j
    for (var k = 0; k < length; k++) {
      if (Object.keys(my_arr[i]).length == 0) { return false }
    }

    return true
  }

  function barchartFun (data) {
    var index = data.length - 1
    var dateval = data[index]

    Highcharts
				.chart(
						'chart',
      {
        chart: {
          type: 'column'
        },

        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          floating: true,
          enabled: true,
          floating: false,
          borderWidth: 1,
          backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
          shadow: true
        },

        plotOptions: {

          series: {

            pointPadding: 0,
            borderWidth: 0,
            animation: {
              duration: 5000
            },
            dataLabels: {
              enabled: true,
              formatter: function () {
                if (this.y > 0) { return this.y }
              }
            }
          }
        },

        xAxis: {
          categories: dateval,
          labels: {
            enabled: false
          },

          tickLength: 40
        },

        yAxis: {
          title: {
            text: 'Record Counts'
          }
        },

        title: {
          text: 'Record Counts',
          style: {
            fontWeight: 'bold',
            'fontSize': '14px'

          }
        },
        series: data
      })
  }
  function MixlineChartFun (data) {
    var interval = 0
    var index = data.length - 1
    var dateval = data[index]
    index = data.length - 2
    var datetoarray = data[index]
    var todate = datetoarray[0]
    index = data.length - 3
    var datefromarray = data[index]
    var fromdate = datefromarray[0]
    var len = dateval.length
    if (len < 2) {
      len = 0
    } else {
      len = len - 1
    }
    delete data[data.length - 3]
    delete data[data.length - 2]
    delete data[data.length - 1]
    var j = 0
    for (var i in data) {
      j++
    }
    var length = j
    var val = checkArray(data)
    if (val) {
      $('#errmsg').hide()
      $('#button').show()
      $('#ULtab').show()
      $('#chart').show()

      if (len > 15) {
        interval = 15
      }

      var options = {
        chart: {
          renderTo: 'chart'
				/*
				 * events: { load: function(){ this.showLoading(); } }
				 */
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          enabled: true,
          borderWidth: 1,
          backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
          shadow: true
				// reversed: true,

        },

        xAxis: {
          min: 0,
          max: len,
          categories: dateval,

          scrollbar: {
            enabled: true
          },
          tickLength: 0,
          tickInterval: interval

        },

        yAxis: {
          title: {
            text: 'Record Counts'
          },
          reversedStacks: false,
          plotLines: [ {
            value: 0,
            width: 1,
            color: '#808080'
          } ]
        },

        tooltip: {
          pointFormat: '<span style="color:{series.color}">{series.name}</span>:<b>{point.y}</b><br/>'
				// valueDecimals: 2
        },

        title: {
          text: 'Record Counts from ' + fromdate + ' to ' + todate,
          style: {
            fontWeight: 'bold',
            'fontSize': '14px'

          },
          x: -20
        },

        plotOptions: {

          series: {
            allowPointSelect: true,
            dashstyle: 'ShortDash',
            cursor: 'pointer',

						/*
						 * zoneAxis: 'x', zones: [{ value: 3 }, { dashStyle:
						 * 'dot' }],
						 */
            events: {
              legendItemClick: function (event) {
                var tablename = this.name
                legendClickFunction(tablename)
              },
              click: function (event) {
                var tablename = this.name
                legendClickFunction(tablename)
              }
            }
          }
        },
        series: [ {
          color: Highcharts.getOptions().colors[2]
        }

				/*
				 * { value: 8 }, { dashStyle: 'dot' }
				 */
        ]
			/*
			 * series: [{ zones: [{ value: 0, color: '#f7a35c', style: 'dotted', }, {
			 * value: 10, color: '#7cb5ec' }, { color: '#90ed7d' }, ] }],
			 */
			/*
			 * series: [{ zoneAxis: 'x', zones: [{ value: 3 }, { dashStyle:
			 * 'dot' }]} ],
			 */

      }

      for (i = 0; i < length; i++) {
        console.log(data[i])
        options.series[i] = data[i]
      }

      if (i == length) {
        chart = new Highcharts.Chart(options)
				/*
				 * $('#chart').highcharts({ series: [{ data: [29.9, 71.5, 106.4,
				 * 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
				 * zoneAxis: 'x', zones: [{ value: 8 }, { dashStyle: 'dot' }] }]
				 * });
				 */
        chart.hideLoading()
      }

      $('.highcharts-xaxis-labels text').on('click', function () {
        var label = $(this).text()

        chartFunction(label, datefrom, dateto)
      })
    } else {
      if (selectedvalue != undefined) {
        document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
						selectedvalue + ' from ' + fromdate + ' to ' + todate
      } else if (label != undefined) {
        document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
						label + ' from ' + fromdate + ' to ' + todate
      } else {
        document.getElementById('errmsg').innerHTML = 'Sorry no data found for ' +
						tablename + ' from ' + fromdate + ' to ' + todate
      }

      $('#errmsg').show()
      $('#button').hide()
      $('#ULtab').hide()
      $('#chart').hide()
			// $('#Datachart').hide();
    }
  }

  function OpenDivforBreadCrumb (evt, key, htitle, vtitle, label) {
    var i, tabcontent, tablinks
    keydata = cache.get(key)
    jsondata = JSON.parse(keydata)
    charttype = jsondata.type
    data = jsondata.data
    if (charttype == 'linechart') { lineChartFun(data) } else if (charttype == 'Mixlinechart') { MixlineChartFun(data) } else			{ barchartFun(data) }
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = 'none'
    }

    tablinks = document.getElementsByClassName('tablinks')
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className
					.replace(' active', '')
    }

    document.getElementById(key).style.display = 'block'
    evt.currentTarget.className += ' active'
  }
}

/*
 * events: { load: function() {
 * //document.getElementById('Datachart').style.background = 'none';
 * //$('#loading').hide(); $('#chart').show(); //$('#Datachart').hide(); } }
 */
