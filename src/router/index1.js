import Vue from 'vue'
import Router from 'vue-router'
import Moment from 'moment'
import MomentTimezone from 'moment-timezone'
import Register from '@/components/Register'
import EnvDashboard from '@/components/EnvDashboard'
import EdcDashboard from '@/components/EdcDashboard'
import Streamer from '@/components/Streamer'
import EnvironmentList from '@/components/AutomateDBEnvironment/EnvironmentList'
import CreateEnviroment from '@/components/AutomateDBEnvironment/CreateEnvironment'
import CombinedViewAccess from '@/components/AutomateDBEnvironment/CombinedViewAccess'
import CreateCombinedView from '@/components/AutomateDBEnvironment/CreateCombinedView'
import CombinedViewEnvironmentList from '@/components/AutomateDBEnvironment/CombinedViewEnvironmentList'
import ShowEnvironments from '@/components/AutomateDBEnvironment/ShowEnvironments'
import ShowEnvironmentList from '@/components/AutomateDBEnvironment/ShowEnvironmentList'
import EdcLogin from '@/components/EdcLogin'
import EnvLogin from '@/components/EnvLogin'

import DataSourceList from '@/components/DataSource/DatasourceList'
import ManageDataSource from '@/components/DataSource/ManageDatasource'
import addConnection from '@/components/DataSource/addConnection'
import addListner from '@/components/DataSource/addListner'

import ManageUser from '@/components/UserProvisioning/ManageUser'
import GetUser from '@/components/UserProvisioning/GetUser'
import UserProfile from '@/components/userProfile'
import ChangePassword from '@/components/changepassword'
import addBulkUser from '@/components/UserProvisioning/addBulkUser'
import UserGuide from '@/components/UserProvisioning/UserGuide'
import Userlist from '@/components/UserProvisioning/UserList'

Vue.use(Router)
Vue.use(Moment)
Vue.use(MomentTimezone)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/edclogin',
      name: 'edclogin',
      component: EdcLogin
    },
    {
      path: '/envlogin',
      name: 'envlogin',
      component: EnvLogin
    },
    {
      path: '/edcdashboard',
      name: 'edcdashboard',
      component: EdcDashboard,
      meta: {
        breadcrumb: 'Home'
      }
    },
    {
      path: '/envdashboard',
      name: 'envdashboard',
      component: EnvDashboard,
      meta: {
        breadcrumb: 'Home'
      }
    },
    {
      path: '/streamer',
      name: 'streamer',
      component: Streamer,
      meta: {
        breadcrumb: 'Home / Streamer'
      }
    },
   {
     path: '/createenvironment',
     name: 'createenvironment',
     component: CreateEnviroment,
     meta: {
       breadcrumb: 'Home / Create Environment'
     }  
    },
   {
     path: '/combinedviewaccess',
     name: 'combinedviewaccess',
     component: CombinedViewAccess,
     meta: {
       breadcrumb: 'Home / Combined View Access'
     }
   },
   {
     path: '/createcombinedview',
     name: 'createcombinedview',
     component: CreateCombinedView,
     meta: {
       breadcrumb: 'Home / Create Combined View / Create Combined View Environment'
     }
   },
   {
     path: '/combinedviewenvlist',
     name: 'combinedviewenvlist',
     component: CombinedViewEnvironmentList,
     meta: {
       breadcrumb: 'Home / Create Combined View'
     }
   },
    {
     path: '/editenvironment',
     name: 'editenvironment',
     component: ShowEnvironments,
     meta: {
       breadcrumb: 'Home / Show Environment View / Edit Environment Environment'
     }
   },
   {
     path: '/showenvlist',
     name: 'showenvlist',
     component: ShowEnvironmentList,
     meta: {
       breadcrumb: 'Home / Show Environment View'
     }
   },
   {
      path: '/datasources',
      name: 'datasources',
      component: DataSourceList,
      meta: {
        breadcrumb: 'Home / Data Sources'
      }
    },
    {
      path: '/createdatasource',
      name: 'createdatasource',
      component: ManageDataSource,
      meta: {
        breadcrumb: 'Home / Data Sources / Create Data Source'
      }
    },
    {
      path: '/addConnectionString',
      name: 'addConnectionString',
      component: addConnection,
      meta: {
        breadcrumb: 'Home / Data Sources / Add Connection String'
      }
    },
    {
      path: '/:datasourceid/editdatasource',
      name: 'editdatasource',
      component: ManageDataSource,
      meta: {
        breadcrumb: 'Home / Data Sources / Edit Data Source'
      }
    },
    {
      path: '/addlistner',
      name: 'addlistner',
      component: addListner,
      meta: {
        breadcrumb: 'Home / Data Sources / Create Listner'
      }
    },
    {
      path: '/addUser',
      name: 'adduser',
      component: ManageUser,
      meta: {
        breadcrumb: 'Home / User List / Add User'
      }
    },
    {
      path: '/updateUser/:userid',
      name: 'updateuser',
      component: ManageUser,
      meta: {
        breadcrumb: 'Home / User List / Update User'
      }
    },
    {
      path: '/getUser/:userid',
      name: 'getuser',
      component: GetUser,
      meta: {
        breadcrumb: 'Home / User List / Get User'
      }
    },
    {
      path: '/getmyprofile/:userid',
      name: 'myprofile',
      component: UserProfile,
      meta: {
        breadcrumb: 'Home / My Profile'
      }
    },
    {
      path: '/changepassword',
      name: 'changepassword',
      component: ChangePassword,
      meta: {
        breadcrumb: 'Home / Change Password'
      }
    },
    {
      path: '/addBulkUser',
      name: 'addbulkuser',
      component: addBulkUser,
      meta: {
        breadcrumb: 'Home / User List / Add Bulk User'
      }
    },
    {
      path: '/user_provisioning_guide',
      name: 'userguide',
      component:UserGuide,
      meta: {
        breadcrumb: 'Home / User Guide'
      }
    },
    {
      path: '/userList',
      name: 'userlist',
      component: Userlist,
      meta: {
        breadcrumb: 'Home / User List'
      }
    },
  ]
})
