import Vue from 'vue'
import Router from 'vue-router'
import Moment from 'moment'
import MomentTimezone from 'moment-timezone'
// import Hello from '@/components/Hello'
import login from '@/components/login'
import Register from '@/components/Register'
// import checkingregister from '@/components/checkingregister'
import Dashboard from '@/components/Dashboard'
import AssetList from '@/components/Authorization/AssetList'
import SecurityRoleList from '@/components/Authorization/SecurityRoleList'
import ManageSecurityRole from '@/components/Authorization/ManageSecurityRole'
import RoleAsset from '@/components/Authorization/RoleAsset'
import AuthMapList from '@/components/Authorization/AuthMapList'
import ManageAuthMap from '@/components/Authorization/ManageAuthMap'
import AuthorizationGuide from '@/components/Authorization/AuthorizationGuide'
import example from '@/components/example'
import Userlist from '@/components/UserProvisioning/UserList'
import ManageAccount from '@/components/AccountManagement/ManageAccount'
import AccountManagement from '@/components/AccountManagement/AccountsManagement'
import ActivityLogs from '@/components/AccountManagement/ActivityLogs'
import AccountManagementSettings from '@/components/AccountManagement/AccountManagementSettings'
import AccountManagementGuide from '@/components/AccountManagement/AccountManagementGuide'
import OrganizationInfo from '@/components/AccountManagement/OrganizationInfo'
import Streamer from '@/components/Streamer'
import ManageUser from '@/components/UserProvisioning/ManageUser'
import GetUser from '@/components/UserProvisioning/GetUser'
import UserProfile from '@/components/userProfile'
import ChangePassword from '@/components/changepassword'
import addBulkUser from '@/components/UserProvisioning/addBulkUser'
import UserGuide from '@/components/UserProvisioning/UserGuide'
import Download from '@/components/download'
import Packages from '@/components/AccountManagement/packages'
import Subscribe from '@/components/AccountManagement/subscribe'
import Agent from '@/components/Reporting/Agent'
import AgentGuide from '@/components/Reporting/AgentGuide'
import RecordReport from '@/components/Reporting/RecordReport'
import ReportSnapshot from '@/components/Reporting/ReportSnapshot'
import JDSnapshot from '@/components/Reporting/JDSnapshot'
import JDReport from '@/components/Reporting/JDReport'
import Calendar from '@/components/Scheduler/Calendar'
import Rules from '@/components/Scheduler/RuleList'
import ManageRule from '@/components/Scheduler/ManageRule'
import DataSourceList from '@/components/DataSource/DatasourceList'
import ManageDataSource from '@/components/DataSource/ManageDatasource'
import addConnection from '@/components/DataSource/addConnection'
import addListner from '@/components/DataSource/addListner'
import addJDEDataSource from '@/components/DataSource/addJDEDataSource'
import addEnvironment from '@/components/Environment/addEnvironment'
import addCatalog from '@/components/Catalog/addCatalog'
import CatalogList from '@/components/Catalog/CatalogList'
import EnvironmentList from '@/components/Environment/EnvironmentList'

import CreateEnviroment from '@/components/AutomateDBEnvironment/CreateEnvironment'
import CombinedViewAccess from '@/components/AutomateDBEnvironment/CombinedViewAccess'
import CreateCombinedView from '@/components/AutomateDBEnvironment/CreateCombinedView'
import CombinedViewEnvironmentList from '@/components/AutomateDBEnvironment/CombinedViewEnvironmentList'
import ShowEnvironments from '@/components/AutomateDBEnvironment/ShowEnvironments'
import ShowEnvironmentList from '@/components/AutomateDBEnvironment/ShowEnvironmentList'
import ConnectToJde from '@/components/AutomateDBEnvironment/ConnectToJde'
import CreateDataSource from '@/components/AutomateDBEnvironment/CreateDataSource'
import ShowDataSourceList from '@/components/AutomateDBEnvironment/ShowDataSourceList'


Vue.use(Router)
Vue.use(Moment)
Vue.use(MomentTimezone)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        breadcrumb: 'Home'
      }
    },
    {
      path: '/assetlist',
      name: 'assetlist',
      component: AssetList,
      meta: {
        breadcrumb: 'Home / Asset List'
      }
    },
    {
      path: '/securityrolelist',
      name: 'securityrolelist',
      component: SecurityRoleList,
      meta: {
        breadcrumb: 'Home / Security Role List'
      }
    },

    {
      path: '/roleasset',
      name: 'roleasset',
      component: RoleAsset,
      meta: {
        breadcrumb: 'Home / Role Asset'
      }
    },

    {
      path: '/authorization_guide',
      name: 'authorizationguide',
      component: AuthorizationGuide,
      meta: {
        breadcrumb: 'Home / Authorization Guide'
      }
    },

    {
      path: '/securityrolecreate',
      name: 'securityrolecreate',
      component: ManageSecurityRole,
      meta: {
        breadcrumb: 'Home / Security Role List / Security Role Create'
      }
    },
    {
      path: '/:securityroleid/SecurityRoleEdit',
      name: 'securityroleedit',
      component: ManageSecurityRole,
      meta: {
        breadcrumb: 'Home / Security Role List / Security Role Edit'
      }
    },
    {
      path: '/authmaplist',
      name: 'authmaplist',
      component: AuthMapList,
      children: [
      ]
    },
    {
      path: '/authmapcreate',
      name: 'authmapcreate',
      component: ManageAuthMap
    },
    {
      path: '/example',
      name: 'example',
      component: example
    },
    {
      path: '/userList',
      name: 'userlist',
      component: Userlist,
      meta: {
        breadcrumb: 'Home / User List'
      }
    },
    {
      path: '/AccountManagement',
      name: 'accountmanagement',
      component: AccountManagement,
      meta: {
        breadcrumb: 'Home / Account Management'
      }
    },
    {
      path: '/AccountManagementGuide',
      name: 'accountManagementGuide',
      component: AccountManagementGuide,
      meta: {
        breadcrumb: 'Home / Account Management / User Guide'
      }
    },
    {
      path: '/ActivityLogs',
      name: 'activitylogs',
      component: ActivityLogs,
      meta: {
        breadcrumb: 'Home / Activity Logs'
      }
    },
    {
      path: '/AccountManagementSettings',
      name: 'accountmanagementsettings',
      component: AccountManagementSettings,
      meta: {
        breadcrumb: 'Home / Account Management'
      }
    },
    {
      path: '/organizationinfo',
      name: 'organizationinfo',
      component: OrganizationInfo,
      meta: {
        breadcrumb: 'Home / Account Management'
      }
    },
    {
      path: '/packages',
      name: 'packages',
      component: Packages,
      meta: {
        breadcrumb: 'Home / Account Management'
      }
    },
    {
      path: '/subscribe/:packageid',
      name: 'subscribe',
      component: Subscribe,
      meta: {
        breadcrumb: 'Home / Account Management'
      }
    },
    {
      path: '/manageaccount',
      name: 'manageaccount',
      component: ManageAccount,
      meta: {
        breadcrumb: 'Home / Organization Details / Edit Preferences'
      }
    },
    {
      path: '/streamer',
      name: 'streamer',
      component: Streamer,
      meta: {
        breadcrumb: 'Home / Streamer'
      }
    },
    {
      path: '/addUser',
      name: 'adduser',
      component: ManageUser,
      meta: {
        breadcrumb: 'Home / User List / Add User'
      }
    },
    {
      path: '/updateUser/:userid',
      name: 'updateuser',
      component: ManageUser,
      meta: {
        breadcrumb: 'Home / User List / Update User'
      }
    },
    {
      path: '/getUser/:userid',
      name: 'getuser',
      component: GetUser,
      meta: {
        breadcrumb: 'Home / User List / Get User'
      }
    },
    {
      path: '/getmyprofile/:userid',
      name: 'myprofile',
      component: UserProfile,
      meta: {
        breadcrumb: 'Home / My Profile'
      }
    },
    {
      path: '/changepassword',
      name: 'changepassword',
      component: ChangePassword,
      meta: {
        breadcrumb: 'Home / Change Password'
      }
    },
    {
      path: '/addBulkUser',
      name: 'addbulkuser',
      component: addBulkUser,
      meta: {
        breadcrumb: 'Home / User List / Add Bulk User'
      }
    },
    {
      path: '/user_provisioning_guide',
      name: 'userguide',
      component:UserGuide,
      meta: {
        breadcrumb: 'Home / User Guide'
      }
    },
    {
      path: '/download',
      name: 'download',
      component: Download,
      meta: {
        breadcrumb: 'Home / Download'
      }
    },
    {
      path: '/Agent',
      name: 'agent',
      component: Agent,
      meta: {
        breadcrumb: 'Home / Reporting / Agent'
      }
    },
    {
      path: '/AgentGuide',
      name: 'agentGuide',
      component: AgentGuide,
      meta: {
        breadcrumb: 'Home / Reporting / Agent / AgentGuide'
      }
    },
    {
      path: '/Snapshots',
      name: 'reportsnapshot',
      component: ReportSnapshot,
      meta: {
        breadcrumb: 'Home / Reports / Report Snapshot'
      }
    },
    {
      path: '/Reports',
      name: 'recordreport',
      component: RecordReport,
      meta: {
        breadcrumb: 'Home / Reporting / Record Report'
      }
    },
    {
      path: '/JDReport',
      name: 'jdreport',
      component: JDReport,
      meta: {
        breadcrumb: 'Home / Reports / JD Report'
      }
    },
    {
      path: '/JDSnapshot',
      name: 'jdsnapshot',
      component: JDSnapshot,
      meta: {
        breadcrumb: 'Home / Reporting / JD Snapshot'
      }
    },
    {
      path: '/calendar',
      name: 'calendar',
      component: Calendar,
      meta: {
        breadcrumb: 'Home / Calendar'
      }
    },
    {
      path: '/rules',
      name: 'rules',
      component: Rules,
      meta: {
        breadcrumb: 'Home / Rules'
      }
    },
    {
      path: '/createrule',
      name: 'createrule',
      component: ManageRule,
      meta: {
        breadcrumb: 'Home / Rules / Create Rule'
      }
    },
       {
      path: '/:ruleid/editrule',
      name: 'editrule',
      component: ManageRule,
      meta: {
        breadcrumb: 'Home / Rules / Edit Rule'
      }
    },
    {
      path: '/datasources',
      name: 'datasources',
      component: DataSourceList,
      meta: {
        breadcrumb: 'Home / Data Sources'
      }
    },
    {
      path: '/createdatasource',
      name: 'createdatasource',
      component: ManageDataSource,
      meta: {
        breadcrumb: 'Home / Data Sources / Create Data Source'
      }
    },
    {
      path: '/addConnectionString',
      name: 'addConnectionString',
      component: addConnection,
      meta: {
        breadcrumb: 'Home / Data Sources / Add Connection String'
      }
    },
    {
      path: '/:datasourceid/editdatasource',
      name: 'editdatasource',
      component: ManageDataSource,
      meta: {
        breadcrumb: 'Home / Data Sources / Edit Data Source'
      }
    },
    {
      path: '/addlistner',
      name: 'addlistner',
      component: addListner,
      meta: {
        breadcrumb: 'Home / Data Sources / Create Listner'
      }
    },
    {
      path: '/environment',
      name: 'environment',
      component: EnvironmentList,
      meta: {
        breadcrumb: 'Home / Environment'
      }
    },
    {
      path: '/addEnvironment',
      name: 'addEnvironment',
      component: addEnvironment,
      meta: {
        breadcrumb: 'Home / Environment / Create Environment'
      }
    },
    {
      path: '/catalog',
      name: 'catalog',
      component: CatalogList,
      meta: {
        breadcrumb: 'Home / Catalog'
      }
    },
    {
      path: '/addCatalog',
      name: 'addCatalog',
      component: addCatalog,
      meta: {
        breadcrumb: 'Home / Catalog / Create Catalog'
      }
    },

    {
     path: '/createenvironment',
     name: 'createenvironment',
     component: CreateEnviroment,
     meta: {
       breadcrumb: 'Home / Create Environment'
     }  
    },
   {
     path: '/combinedviewaccess',
     name: 'combinedviewaccess',
     component: CombinedViewAccess,
     meta: {
       breadcrumb: 'Home / Combined View Access'
     }
   },
    {
     path: '/createcombinedview',
     name: 'createcombinedview',
     component: CombinedViewEnvironmentList,
     meta: {
       breadcrumb: 'Home / Create Combined View'
     }
   },
   {
     path: '/editcombinedview',
     name: 'editcombinedview',
     component: CreateCombinedView,
     meta: {
       breadcrumb: 'Home / Create Combined View / Edit Combined View'
     }
   },
   {
     path: '/showenvironment',
     name: 'showenvironment',
     component: ShowEnvironmentList,
     meta: {
       breadcrumb: 'Home / Show Environment'
     }
   },
   {
     path: '/editenvironment',
     name: 'editenvironment',
     component: ShowEnvironments,
     meta: {
       breadcrumb: 'Home / Show Environment / Edit Environment'
     }
   },
   {
     path: '/jdeapplication',
     name: 'jdeapplication',
     component: ConnectToJde,
     meta: {
       breadcrumb: 'Home / JDE Application'
     }
   },
   {
      path: '/:env_name/editenvironment',
      name: 'editenvironment',
      component: ShowEnvironments,
      meta: {
        breadcrumb: 'Home / Show Environment / Edit Environment'
      }
    },
    {
     path: '/editdatasource',
     name: 'editdatasource',
     component: CreateDataSource,
     meta: {
       breadcrumb: 'Home / Edit Data Source'
     }
    },
    {
     path: '/showdatasource',
     name: 'showdatasource',
     component: ShowDataSourceList,
     meta: {
       breadcrumb: 'Home / Show Data Source'
     }
    },
    {
      path: '/addjdedatasource',
      name: 'addjdedatasource',
      component: addJDEDataSource,
      meta: {
        breadcrumb: 'Home / Data Sources / Add JDE DataSource'
      }
    },
    
  ]
})
