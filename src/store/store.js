import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export const store = new Vuex.Store({
    state: {
        isPasswordExpired: false,
        isJdeApplication: false
    },
    getters: {},
    mutations: {
        // Mutations
        setResetPassword: state => state.isPasswordExpired = true,
        setJdeAppStatus: state => state.isJdeApplication = true
    },
    actions: {
        setResetPassword(context) {
            context.commit('setResetPassword')
        },
        setJdeAppStatus(context) {
            context.commit('setJdeAppStatus')
        }
    }
})