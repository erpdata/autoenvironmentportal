'use strict'
import config from '../config.json'
import apptoken from '../apptoken.json'

export function getServerData(that, endPoint) {
    let authorization = that.$session.get('access_token')
        // let baseUrl = config.DATA_SOURCE_URL
    let headerData = {
        'Authorization': authorization,
        'Content-Type': 'application/json'
    }
    return new Promise(function(resolve, reject) {
        that.$http.get(endPoint, { headers: headerData }).then(response => {
            return resolve(response.bodyText)
        })
    })
}

export function post(that, endPoint, InputJson) {
    let authorization = that.$session.get('access_token') ? that.$session.get('access_token') : apptoken.APP_TOKEN

    // let baseUrl = config.DATA_SOURCE_URL
    let headerData = {
        'Authorization': authorization,
        'Content-Type': 'application/json'
    }
    return new Promise(function(resolve, reject) {
        that.$http.post(endPoint, InputJson, { headers: headerData }, ).then(response => {
            return resolve(response.bodyText)
        })
    })
}