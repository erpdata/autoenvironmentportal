'use strict'
export default function (that) {
  return new Promise(function (resolve, reject) {
    that.$http.get('https://api.ipify.org?format=jsonp'
    ).then(response => {
      var ip = response.bodyText
      var ipadd = JSON.parse(ip.slice(9, -2))
      console.log('IP ADDRESS ARESPONSE')
      console.log(ipadd.ip)
      return resolve(ipadd.ip)
    }, (response) => {
      console.log(response)
    })
  })
};
