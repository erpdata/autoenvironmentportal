'use strict'
import config from '../config.json'
import redirectToLogin from './loginPage'
export default function (that) {
  let isLoggedIn = that.$session.exists()
  if (isLoggedIn) {
    if (that.$cookies.isKey('Timeout')) { // check iscookie exists
        //  means session not yet get timeout
        //  so delete this cookie and again save new one.So timer will get reset
      that.$cookies.remove('Timeout')
        //  set new cookie with timeout. refresh timeout
      that.$cookies.set('Timeout', 'timeout', config.SESSION_TIMEOUT + 'min')
    } else {  // cookie not exist means session get timeout.So destroy the session manually
      that.$session.destroy()
      redirectToLogin()
    }
  } else { // session not exists. So redirect to login
      // remove previously exists cookie . if browser get closed without logout.
    if (that.$cookies.isKey('Timeout')) {
      that.$cookies.remove('Timeout')
    }
    if (that.$cookies.isKey('SessionExpMsg')) {
      that.$cookies.remove('SessionExpMsg')
    }
    that.$cookies.set('SessionExpMsg', 'msg', '1min')
    redirectToLogin()
  }
}
