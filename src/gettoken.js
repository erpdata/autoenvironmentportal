
var server = require('../build/dev-server.js')
var config = require('./config.json')
var request = require('request')
var fs = require('fs')

server.ready.then(() => {
  request.post(
	    config.AUTHENTICATION_URL + '/add_application',
	    { json: { application_name: config.SERVICE_ID } },
	    function (error, response, body) {
	        if (!error && response.statusCode == 200) {
	            console.log(body.access_token)
	            fs.writeFile('./src/apptoken.json', '{"APP_TOKEN":"' + body.access_token + '"}', function (err) {
				    if (err) {
				        return console.log(err)
				    }
            })
	        }
	    }
)
})
