// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.

import Vue from 'vue'

import Moment from "moment";

import MomentTimezone from "moment-timezone";
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VeeValidate from 'vee-validate'
import VueSession from 'vue-session'
import VueBreadcrumbs from 'vue2-breadcrumbs'
import VueCookies from 'vue-cookies'
import Vuex from 'vuex'
import VueLadda from 'vue-ladda'
import VuePassword from 'vue-password'
import { store } from './store/store'


window.Inputmask = require('inputmask');

Vue.directive('input-mask', {
    bind: function(el) {
        new Inputmask({
            mask: $(el).attr('mask'),
        }).mask(el);
    },
});
var config = require('./config.json')
Vue.config.productionTip = false

var cors = require('cors')
Vue.use(VueResource)
Vue.use(VeeValidate)
Vue.use(VueSession)
Vue.use(VueBreadcrumbs)
Vue.use(VueCookies)
Vue.use(Moment)
Vue.use(MomentTimezone)
Vue.use(Vuex)
Vue.component('vue-ladda', VueLadda)
Vue.component('vue-password', VuePassword)
Vue.mixin({
    data() {
        return {
            MainMenuList: [],
            start_tour: false
        }
    },

    methods: {
        LoggerData: function(logjson) {
            console.log(logjson)
            this.$http.post(config.LOGGER_URL, logjson).then((response) => {
                console.log("response from logger")
                console.log(logjson)
                console.log(response)
            })
        },
        errorLogger: function(logjson, e, starttime) {
            logjson.payload = e.url + "status:" + e.status + "statsutext:" + e.statusText
            logjson.runtime = (Math.trunc((new Date()).getTime()) - starttime) + "ms"
            logjson.severity = "ERROR"
            this.LoggerData(logjson)
            console.log("errorlog")
        },
        responseLogger: function(logjson, response, starttime) {
            logjson.runtime = (Math.trunc((new Date()).getTime()) - starttime) + "ms"
            logjson.payload = response.url + " status:" + response.status + " statsutext: " + response.statusText
            logjson.severity = "INFO"
            this.LoggerData(logjson)

            console.log("responselog")
        },

        getReportAssest: function() {
            console.log("**ReportAssest**")
            var Authorization = this.$session.get('access_token')
            return this.$http.get(config.AUTHORIZATION_URL + '/auth_maps/get_assets/Reporting', {
                headers: {
                    'Authorization': Authorization
                }
            }).then(response => {
                console.log("**reponse**")
                var data = JSON.parse(response.bodyText)

                if (data.status === 200) {
                    this.ReportMenuList = data
                    console.log(data)

                } else {
                    console.log(data)
                }
            }, (response) => {
                console.log("**error**")
                console.log(response)
            })
        },

        MainCheckUrl: function() {
            this.MainMenuList = []
            if (this.$route.path === '/userlist' || this.$route.path === '/user_provisioning_guide') {
                this.MainMenuList.push({
                    "MenuName": "User Provisioning",
                    "url": "/userlist",
                    "SubMenu": [{ "SubMenuName": "User Provisioning Guide", "suburl": "/user_provisioning_guide", "SubMenuId": "user_guide" }]
                })
                console.log('userlist')
            }
            // if (this.$route.path === '/dashboard') {

            //     this.getReportAssest().then(() => {
            //         this.SubMenu = []
            //         this.MenuUrl = "/Agent"
            //         this.MenuName = "Report"
            //         for (var i = 0; typeof this.ReportMenuList.data != 'undefined' && i < this.ReportMenuList.data.length; i++) {
            //             this.SubMenu.push({
            //                 "SubMenuName": this.ReportMenuList.data[i].module_id,
            //                 "suburl": this.ReportMenuList.data[i].asset_endpoint
            //             })
            //         }
            //         this.MainMenuList.push({ "MenuName": "Reporting", "url": "/Agent", "SubMenu": this.SubMenu })
            //     })
            // }
        },
    }
});

var consul = require('consul')({ 'host': config.CONSUL_IP_ADDRESS, 'port': config.CONSUL_PORT })

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
})
